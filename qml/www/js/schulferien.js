var weekDays = [ "Mo", "Di", "Mi", "Do", "Fr", "Sa", "So" ];
var months = [ "Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez" ];
var darkmode = false;
var icsContent = "";

function init() {
    var darkmodeSetting = localStorage.getItem("darkmode");
    if (darkmodeSetting != null) {
        darkmode = (darkmodeSetting == "1");
        if (darkmode)
            setDarkMode();
    }
    var date = new Date();
    var state = localStorage.getItem("state");
    if (state != null)
        document.getElementById("state").value = state;
    document.getElementById("year").value = date.getFullYear();
    getData();
}

function parseDate(stringValue) {
    var helper = stringValue.split("-");
    return new Date(helper[0], (helper[1] * 1) - 1, helper[2]);
}

function getDateString(stringValue) {
    var helper = stringValue.split("-");
    var date = new Date(helper[0], (helper[1] * 1) - 1, helper[2]);
    return date.getDate() + ". " + months[date.getMonth()] + ".";
}

function getIcsDateString(stringValue) {
    var helper = stringValue.split("-");
    var date = new Date(helper[0], (helper[1] * 1) - 1, helper[2]);
    var year = date.getFullYear().toString();
    var month = (date.getMonth() + 1).toString();
    var day = date.getDate().toString();
    if (month.length == 1)
        month = "0" + month;
    if (day.length == 1)
        day = "0" + day;
    return year + month + day;
}

function formatName(value) {
    var helper = value.split(" ");
    var name = helper[0];
    return name.substr(0, 1).toUpperCase() + name.substr(1, name.length - 1);
}

function getData() {
    icsContent = "";
    var today = new Date();
    document.getElementById("currentdate").innerHTML = today.toLocaleDateString();
    document.getElementById("main-content").innerHTML = "";
    var http = new XMLHttpRequest();
    var state = document.getElementById("state").value;
    var year = document.getElementById("year").value;
    localStorage.setItem("state", state);
    var url = "https://ferien-api.de/api/v1/holidays/" + state + "/" + year;
    http.open("GET", url);
    http.send();
    http.onload = function() {
        if (http.status == 200) {
            var response = http.responseText;
            var data = JSON.parse(response);
            if (data != null && data.length > 0) {
                var lastGroup = "-";
                var htmlContent = "";
                icsContent += "BEGIN:VCALENDAR\n";
                for (var i = 0; i < data.length; i++) {
                    if (lastGroup != formatName(data[i].name)) {
                        lastGroup = formatName(data[i].name);
                        htmlContent += "<div class=\"group-header\">" + formatName(data[i].name) + "</div>";
                    }
                    var groupDetailsExt = "";
                    var checkEndDate = parseDate(data[i].end);
                    checkEndDate.setHours(23);
                    checkEndDate.setMinutes(59);
                    checkEndDate.setSeconds(00);
                    if (today >= parseDate(data[i].start) && today <= checkEndDate)
                        groupDetailsExt = "today";
                    else if (today > checkEndDate)
                        groupDetailsExt = "past";
                    htmlContent += "<div class=\"group-detail " + groupDetailsExt + " \">"
                                 + "von <b>" + weekDays[parseDate(data[i].start).getDay() - 1] + ". " + getDateString(data[i].start) + "</b> "
                                 + "bis <b>" + weekDays[parseDate(data[i].end).getDay() - 1] + ". " + getDateString(data[i].end) + "</b> "
                                 + "</div>";
                    icsContent += "BEGIN:VEVENT\n"
                                + "SUMMARY:" + formatName(data[i].name) + "\n"
                                + "DTSTART;VALUE=DATE:" + getIcsDateString(data[i].start) + "\n"
                                + "DTEND;VALUE=DATE:" + getIcsDateString(data[i].end) + "\n"
                                + "END:VEVENT\n";
                }
                document.getElementById("main-content").innerHTML = htmlContent;
                icsContent += "END:VCALENDAR\n";
            }
            else {
                document.getElementById("main-content").innerHTML = "Keine Daten für diesen Zeitraum gefunden.";
            }
            console.log(response);
        }
        else {
            document.getElementById("main-content").innerHTML = "Beim Abrufen der Daten gab es ein Problem (HTTP-Status " + http.status + ") ;-(";
        }
    }
    http.onerror = function() {
        document.getElementById("main-content").innerHTML = "Beim Abrufen der Daten gab es ein Problem ;-(<br>URL: " + url;
    }
}

function switchDisplayMode() {
    if (!darkmode) {
        darkmode = true;
        setDarkMode();
    }
    else {
        darkmode = false;
        setLightMode();
    }
}

function setDarkMode() {
    localStorage.setItem("darkmode", "1");
    document.getElementById("displaymode").className = "btn btn-light-mode";
    var css = document.querySelectorAll("head > link");
    css[1].href = "css/format-dark.css";
}

function setLightMode() {
    localStorage.setItem("darkmode", "0");
    document.getElementById("displaymode").className = "btn btn-dark-mode";
    var css = document.querySelectorAll("head > link");
    css[1].href = "css/format-light.css";
}

function downloadIcsFile() {
    var state = document.getElementById("state").value;
    var year = document.getElementById("year").value;
    var element1 = document.createElement('a');
    element1.setAttribute('href', 'data:text/plain;,' + icsContent);
    element1.setAttribute('download', "ferien-" + state + "-" + year + ".ics");
    element1.style.display = 'none';
    document.body.appendChild(element1);
  
    element1.click();
  
    document.body.removeChild(element1);
}

function showCredits() {
    document.getElementById("credits-container").style.display = "flex";
}

function hideCredits() {
    document.getElementById("credits-container").style.display = "none";
}