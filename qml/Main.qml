/*
 * Copyright (C) 2022  Daniel Schlieckmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * schulferien is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Morph.Web 0.1
import QtWebEngine 1.7
import Lomiri.DownloadManager 1.2

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'schulferien.daniel'
    automaticOrientation: true

    Page {
        anchors.fill: parent

        WebContext {
            id: mainContext
            onDownloadRequested: {
                // console.log("Download started");
                // console.log("Filename " + download.downloadFileName);
                // console.log("Url: " + download.url);
                //downloadData.download(download.url);
                // var fileUrl = "/home/phablet/Downloads/" + download.downloadFileName;
                var fileUrl = "/home/phablet/.local/share/schulferien.daniel/Downloads/" + download.downloadFileName;
                var request = new XMLHttpRequest();
                request.open("PUT", fileUrl, false);
                request.send(decodeURIComponent(download.url.toString().replace("data:text/plain;,", "")));
                // console.log(download.downloadFileName + " fertig. " + request.status);
                PopupUtils.open(downloadedMessage);
            }
        }

        WebView {
            id: mainWebView
            context: mainContext
            focus: true
            enableSelectOverride: true
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            settings {
                javascriptCanAccessClipboard: true
                localContentCanAccessFileUrls: true
                localContentCanAccessRemoteUrls: true
                allowRunningInsecureContent: true
                allowWindowActivationFromJavaScript : true
                pluginsEnabled: true
            }
            Component.onCompleted: {
                settings.localStorageEnabled = true;
            }

            zoomFactor: units.gu(1) / 8.4
            url: Qt.resolvedUrl('www/index.html')

            onNewViewRequested: {
                request.action = WebEngineNavigationRequest.IgnoreRequest
                if(request.userInitiated) {
                    Qt.openUrlExternally(request.requestedUrl)
                }
            }
            onFeaturePermissionRequested: {
                grantFeaturePermission(securityOrigin, feature, true);
            }

        }

        Component {
            id: downloadedMessage
            Popover {
                id: popover
                Rectangle {
                    color: "dodgerblue"
                    width: popover.width
                    height: units.gu(8)

                    Label {
                        anchors.centerIn: parent
                        text: "Download abgeschlossen."
                        color: "white"
                    }
                }
            }
        }

    }
}
